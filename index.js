console.log('Hello');


let num = 6;

//Create a variable getCube and use the exponent operator to compute for the cube of a number
const getCube = num ** 3
//Using Template Literals, print out the value of the getCube variable
console.log(`The cube of ${num} is ${getCube}`);



//Create a variable address with a value of an array containing details of an address.
const address = ['3911 EDSA cor. Madison St.', `Brgy. Barangka Ilaya`, `Mandaluyong City`];

//Destructure the array and print out a message with the full address using Template Literals.
const [streetNumber, district, city] = address;
console.log(`I live at ${streetNumber}, ${district}, ${city}`);


//Create a variable animal with a value of an object data type with different animal details as it’s properties.
const animal = {
	name: 'Harambe',
	species: 'western gorilla',
	weight: `200 kg`,
	age: 17
}

//Destructure the object and print out a message with the details of the animal using Template Literals.
const {name, species, weight, age} = animal;
console.log(`${name} was a ${age} years old ${species}. He weighed ${weight}.`);

//Create an array of numbers
const numbers = [1,2,3,4,5];

//Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.
numbers.forEach((number) => console.log(number));



//Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.
class Dog {
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}


//Create/instantiate a new object from the class Dog and console log the object.
const doggo = new Dog('Cheems', 11, 'Shiba Inu');
console.log(doggo);